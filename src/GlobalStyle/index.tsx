import {createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    *{
        box-sizing:border-box;
        margin:0;
    }
    body {
        background:#fff;
    }
    html,body{
        scroll-behavior: smooth;
    }
    a{
        text-decoration:none;
        color:inherit;
    }
`;
 
export default GlobalStyle;
