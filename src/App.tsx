import React, { Fragment } from 'react';
import GlobalStyle from './GlobalStyle/';
import GlobalFonts from './fonts/fonts.js';
import MainPage from './pages/Main/';
import Registration from './pages/Registration/';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <GlobalFonts/>
      <GlobalStyle/>
      <Route path="/registration">
        <Registration/>
      </Route>
      <Route exact path="/">
        <MainPage/>
      </Route>
    </Router>
  );
}

export default App;
