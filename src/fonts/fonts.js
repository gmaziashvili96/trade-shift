import { createGlobalStyle } from 'styled-components';

import FiraBold from './FiraGO-Bold.otf';
import FiraReg from './FiraGO-Regular.otf';
import FiraExtraBold from './FiraGO-ExtraBold.otf';

export default createGlobalStyle`
     @font-face {
        font-family: "firabold";
        src: local("semibold"),
          url("${FiraBold}") format("truetype");
        font-weight: normal;
    }
    @font-face {
        font-family: "firareg";
        src: local("regular"),
          url("${FiraReg}") format("truetype");
        font-weight: normal;
    }
    @font-face {
        font-family: "firaextra";
        src: local("medium"),
          url("${FiraExtraBold}") format("truetype");
        font-weight: normal;
    }
`;