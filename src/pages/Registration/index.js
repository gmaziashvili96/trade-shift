import styled from 'styled-components';
import { Formik,Field } from 'formik';
import Logo from '../../Components/Logo';
import logoIcon from '../../assets/img/logo-dark.png';
import regBG from '../../assets/img/reg-bg.png';
import earthIcon from '../../assets/img/earth.svg';
import locationIcon from '../../assets/img/location.svg';
import emailIcon from '../../assets/img/mail.svg';
import phoneIcon from '../../assets/img/phone.svg';
import dateIcon from '../../assets/img/date.svg';
import ReCAPTCHA from "react-google-recaptcha";
import device from '../../config/device';

const SingUp = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    width:100%;
    max-width:1920px;
    margin:0 auto;
    @media ${device.lg}{
        flex-direction:column;
    }
`;

const About = styled.div`
    width:100%;
    @media ${device.xl}{
        width:55%;
    }
    @media ${device.lg}{
        width:100%;
    }
`;

const DateIcon = styled.div`
    width:30px;
    height:30px;
    margin-right:auto;
    margin-left:15px;
    flex-shrink:0;
    background:url(${dateIcon})no-repeat center;
    background-size:contain;
`;

const AboutInfo = styled.div`
    padding:50px;
    width:100%;
    background:url(${regBG})no-repeat center center;
    background-size:contain;
    @media ${device.xl}{
        padding:25px;
    }
    @media ${device.lg}{
        background-size:100%;
    }
`;

const AboutText = styled.p`
    font:14px firareg;
    color:#61636B;
    margin-bottom:15px;
    line-height:20px;
    &:nth-child(2){
        margin-top:25px;
    }
`;

const Inline = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;
const AboutList = styled.div`
    width:100%;
    margin-top:400px;
    padding-left:100px;
    @media ${device.lg}{
        margin-top:100px;
        padding-left:50px;
    }
`;
const AboutListHd = styled.div`
    font:16px firabold;
    color:#246BB4;
    padding-left:10px;
    text-transform:uppercase;
`;
const AboutItem = styled.div`
    font:13px firabold;
    color:#61636B;
    padding-left:15px;
    margin-top:15px;
    position: relative;
    &:before{
        position:absolute;
        top:50%;
        transform:translate(0,-50%);
        left:0;
        width:10px;
        height:10px;
        border-radius:50%;
        background:#246bb4;
        content:"";
    }
`;

const Form = styled.section`
    width:100%;
    padding:50px;
    @media ${device.xl}{
        padding:25px;
    }
`;

const Input = styled.input`
    width:100%;
    height: 44px;
    font:17px firareg;
    color:#000;
    border:${props => props.border || '2px solid #257ABE'};
    border-radius:5px;
    outline:none;
    padding:0 10px;
    max-width:${props => props.maxWidth || '230px'};
    @media ${device.sm}{
        max-width:100%;
    }
`;
const Select = styled.select`
    width:100%;
    height: 44px;
    font:17px firareg;
    color:#000;
    border:${props => props.border || '2px solid #257ABE'};
    border-radius:5px;
    outline:none;
    padding:0 10px;
    overflow:hidden;
    max-width:${props => props.maxWidth || '230px'};
    @media ${device.sm}{
        max-width:100%;
    }
`;

const Button = styled.button`
    background:#257ABE;
    border-radius:5px;
    outline:none;
    border:none;
    cursor: pointer;
    padding:0 15px;
    height:38px;
    font-size:14px;
    font-family:firabold;
    color:#fff;
    white-space:nowrap;
    @media ${device.sm}{
        margin-top:20px;
        width:100%;
        height:50px;
    }
`;
const InputWrp = styled.div`
    display: flex;
    flex-direction:column;
    width:100%;
    max-width:230px;
    margin-bottom:15px;
    input,select{
        margin-top:5px;
    }
    @media ${device.sm}{
        max-width:100%;
    }
`;

const Label = styled.label`
    font:13px firabold;
    color:#61636B;
    span{
        color:#EC1E24;
    }
`;

const LabelBig = styled(Label)`
    font-size:16px;
    margin-bottom:25px;
    display: block;
    font-family:firareg;
    input{
        margin-right:8px;
        width:18px;
        height:18px;
        border-radius:2px;
        color: #257ABE;
    }
`;

const Footer = styled.footer`
    width:calc(100% - 20px);
    margin:10px;
    margin-top:100px;
    background:#257ABE;
    padding:20px;
    display: flex;
    justify-content: center;
    align-items: center;
    @media ${device.xl}{
        flex-wrap:wrap;
    }
    @media ${device.lg}{
        flex-wrap:nowrap;
        margin-top:40px;
    }
`;
const FooterWrapper = styled.footer`
    max-width:600px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap:wrap;
`;
const FooterItem = styled.footer`
    width:50%;
    font:13px firareg;
    color:#fff;
    margin:20px 0;
    @media ${device.xl}{
        width:100%;
    }
    @media ${device.lg}{
        width:50%;
    }
`;
const FooterIcon = styled.img`
    margin-right:15px;
    width:15px;
    height:15px;
    object-fit:contain;
    object-position:center;
`;

const Title = styled.h1`
    font:26px firabold;
    color:#246BB4;
    margin-bottom:25px;
`;

const Window = styled.div`
    margin:15px 0;
`;
const Board = styled.div`
    padding:0 15px;
    width:100%;
`;
const InputBoard = styled(Board)`
    max-width:550px;
    display:flex;
    flex-wrap:wrap;
    justify-content:space-between;
    margin-bottom:20px;
`;
const Text = styled.p`
    font:16px firareg;
    color:#61636B;
    margin-bottom:20px;
`;
const Billing = styled.p`
    font:16px firareg;
    color:#246BB4;
    margin-bottom:20px;
`;
const TitleSecondary = styled.h1`
    font:18px firabold;
    color:#246BB4;
    margin-bottom:25px;
`;
const Action = styled.div`
    width:100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin:50px 0;
    @media ${device.sm}{
        flex-direction:column;
    }
`;

const Registration = () => {
    return(
        <SingUp>
            <About>
                <AboutInfo>
                    <Logo color={`url(${logoIcon})no-repeat center`}/>
                    <AboutText>
                        Tradeshift-ში გვჯერა, რომ ბიზნესის ზრდას დიდი გლობალური სარგებლის მოტანა
                        შეუძლია. სწორედ ამიტომ, პირველივე დღიდან მიზნად დავისახეთ
                        მეწარმეთათვის მათი ბიზნესპროცესები გაგვემარტივებინა.
                    </AboutText>
                    <AboutText>
                        ჩვენი სხარტი და მოტივირებული გუნდი გამოირჩევა განსხვავებული აზროვნებით
                        და მის წევრებს არ ეშინიათ გააკეთონ ის, რაც აქამდე არავის გაუბედავს.
                    </AboutText>
                    <AboutText>
                        Tradeshift კლიენტებს მომსახურების ფართო სპექტრს ერთ სივრცეში სთავაზობს,
                        მათ შორის - ყოველდღიურ, ყოველკვირეულ ან ყოველთვიურ ვაჭრობას.
                    </AboutText>
                    <AboutList>
                        <AboutListHd>ჩვენთან თანამშრომლობისას:</AboutListHd>
                        <AboutItem>არ გექნებათ ნაღდი ფულის პრობლემა;</AboutItem>
                        <AboutItem>არ გექნებათ შიშის ან უნდობლობის გრძნობა;</AboutItem>
                        <AboutItem>თქვენი ბიზნესი არ შეფერხდება;</AboutItem>
                        <AboutItem>არ დაკარგავთ ახალ კლიენტებსა და დამატებით შემოსავალს;</AboutItem>
                        <AboutItem>დაზოგავთ თანხებსა და ენერგიას;</AboutItem>
                    </AboutList>
                </AboutInfo>
                <Footer>
                    <FooterWrapper>
                        <FooterItem>
                            <FooterIcon src={earthIcon}/> www.trade-shift.com
                        </FooterItem>
                        <FooterItem>
                            <FooterIcon src={locationIcon}/> ბესარიონ ჟღენტის 16
                        </FooterItem>
                        <FooterItem>
                            <FooterIcon src={emailIcon}/> hello@tradeshift.ge
                        </FooterItem>
                        <FooterItem>
                            <FooterIcon src={phoneIcon}/> 0322 194 494
                        </FooterItem>
                    </FooterWrapper>
                </Footer>
            </About>
            <Form>
                <Title>გაიარეთ რეგისტრაცია</Title>
                <TitleSecondary>შეავსეთ პირადი მონაცემები</TitleSecondary>
                <Formik
                    initialValues={{
                        name:'',
                        surname:'',
                        privateIDNumber:'',
                        mobile:'',
                        LegalAdress:'',
                        email: '',
                        password: '',
                        date: '',
                        sex: '',
                        legalSettle: '',
                        IDNumber: '',
                        Foa: '',
                        adress: '',
                        toggle: false,
                        checked: [],
                    }}
                    validate={values => {
                        const errors = {};
                        if (!values.email) {
                        errors.email = 'Required';
                        } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                        errors.email = 'Invalid email address';
                        }
                        return errors;
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                        setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                        }, 400);
                    }}
                    >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                        /* and other goodies */
                    }) => (
                    <form onSubmit={handleSubmit}>
                    <InputBoard>
                        <InputWrp>
                            <Label tmlFor="name">სახელი<span>*</span></Label>
                            <Input
                                type="text"
                                name="name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.name}
                            />
                        </InputWrp>
                        
                        <InputWrp>
                            <Label tmlFor="surname">გვარი<span>*</span></Label>
                            <Input
                                type="text"
                                name="surname"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.surname}
                            />
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                        <InputWrp>
                            <Label tmlFor="privateIDNumber">პირადი ნომერი<span>*</span></Label>
                            <Input
                                type="text"
                                name="privateIDNumber"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.privateIDNumber}
                            />
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                        <InputWrp>
                            <Label tmlFor="mobile">მობილურის ნომერი<span>*</span></Label>
                            <Input
                                type="text"
                                name="mobile"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.mobile}
                            />
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                        <InputWrp>
                            <Label tmlFor="email">ელ-ფოსტა<span>*</span></Label>
                            <Input
                                type="email"
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                        <InputWrp>
                            <Label tmlFor="LegalAdress">იურიდიული მისამართი<span>*</span></Label>
                            <Input
                                type="text"
                                name="LegalAdress"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.LegalAdress}
                            />
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                        <InputWrp>
                            <Label tmlFor="date">დაბადების თარიღი<span>*</span></Label>
                            <Inline>
                                <Input 
                                    maxWidth="190px"
                                    type="text"
                                    name="date"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.date}
                                />
                                <DateIcon/>
                            </Inline>
                        </InputWrp>
                        <InputWrp>
                            <Label tmlFor="sex">სქესი<span>*</span></Label>
                            <Select 
                                maxWidth="190px"
                                name="color"
                                value={values.color}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                style={{ display: 'block' }}
                                >
                                <option value="" label="" />
                                <option value="male" label="male" />
                                <option value="female" label="female" />
                            </Select>
                        </InputWrp>
                        {errors.password && touched.password && errors.password}
                    </InputBoard>

                    <TitleSecondary>შეავსეთ კომპანიის მონაცემები</TitleSecondary>
                    <InputBoard>
                        <InputWrp>
                            <Label tmlFor="legalSettle">იურიდიული დასახელება<span>*</span></Label>
                            <Input
                                type="text"
                                name="legalSettle"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.legalSettle}
                            />
                        </InputWrp>
                        <InputWrp>
                            <Label tmlFor="IDNumber">საიდენთიფიკაციო კოდი*<span>*</span></Label>
                            <Input
                                type="text"
                                name="IDNumber"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.IDNumber}
                            />
                        </InputWrp>
                        <InputWrp>
                            <Label tmlFor="Foa">საქმიანობის სფერო<span>*</span></Label>
                            <Input
                                type="text"
                                name="Foa"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.Foa}
                            />
                        </InputWrp>
                        <InputWrp>
                            <Label tmlFor="adress">თქვენი თანამდებობა*<span>*</span></Label>
                            <Input
                                type="text"
                                name="adress"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.adress}
                            />
                        </InputWrp>
                    </InputBoard>
                    <Window>
                        <TitleSecondary>თანხმობა</TitleSecondary>
                        <Board>
                            <LabelBig>
                                <Field type="checkbox" name="checked" value="One" />
                                თანხმობა მონაცემების დამუშავების თაობაზე<span>*</span>
                            </LabelBig>
                            <Text>
                                მე, გამოვხატავ თანხმობას და უფლებას ვაძლევ შპს ‘’ტრეიდ შიფტს’’ , საქართველოს
                                კანონმდებლობით დადგენილი წესით და პირობებით, დაამუშაოს წინამდებარე განაცხადში,
                                კრედიტორთან დაცული, ასევე, ჩემ შესახებ საკრედიტო ბიუროებში დაცული და მათ მიერ
                                მოწოდებული ინფორმაცია ჩემი, როგორც მომავალი
                                მსესხებლის/თანამსესხებლის/თავდების/უზრუნველყოფის მიმწოდებლის
                                გადახდისუნარიანობის ანალიზის მიზნით.
                            </Text>
                            <Text>
                                გაცნობიერებული მაქვს საქართველოს კანონმდებლობით გათვალისწინებული უფლებები, რომ
                                მომხმარებლის (მათ შორის, მონაცემთა სუბიექტი) მოთხოვნის შემთხვევაში, მონაცემთა
                                დამმუშავებელი ვალდებულია გაასწოროს, განაახლოს, დაამატოს, დაბლოკოს, წაშალოს ან
                                გაანადგუროს მონაცემები, თუ ისინი არასრულია, არაზუსტია, არ არის განახლებული, ან თუ მათი
                                შეგროვება და დამუშავება განხორციელდა კანონის საწინააღმდეგოდ.
                            </Text>
                            <Text>აღნიშნული თანხმობა არის ერთჯერადი და მოქმედია ხელმოწერიდან 30 სამუშაო დღის
                            განმავლობაში (იურიდიული პირის შემთხვევაში 60 სამუშაო დღის განმავლობაში).</Text>
                        </Board>
                    </Window>
                    <Window>
                        <TitleSecondary>იდენთიფიკაცია</TitleSecondary>
                        <Board>
                            <Text>
                                იმისათვის რომ მიიღოთ პირველადი პასუხი ფილიალში მოსვლის გარეშე, პირადი მონაცემების
                                იდენთიფიკაციისთვის გთხოვთ თქვენი ინტენეტ ბანკით გადმორიცხოთ 1 თეთრი ანგარიშზე:
                            </Text>
                            <Billing>ანგარიშის ნომერი - GE23BG7033536020100013</Billing>
                            <Text>იდენთიფიკაციის შემდგომ ჩვენი საკრედიტო ექსპერტი დაგიკავშირდებათ განაცხადში
                            მითითებულ ნომერზე.</Text>
                        </Board>
                    </Window>
                    <Action>
                        <ReCAPTCHA
                            sitekey="Your client site key"
                        />    
                        <Button type="submit" disabled={isSubmitting}>
                            განაცხადის გაგზავნა
                        </Button>
                    </Action>
                    </form>
                )}
                </Formik>
            </Form>
        </SingUp>    
    )
}

export default Registration;