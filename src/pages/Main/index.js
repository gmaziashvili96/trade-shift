import styled from 'styled-components';
import Header from '../../Components/Header/';
import { Fragment } from 'react';
import companyBG from '../../assets/img/bg-header.jpg';
import feedbackBG from '../../assets/img/footer.jpg';
import OffersIcon1 from '../../assets/img/svg-1.svg';
import OffersIcon2 from '../../assets/img/svg-2.svg';
import OffersIcon3 from '../../assets/img/svg-3.svg';
import OffersIcon4 from '../../assets/img/svg-4.svg';
import BgPart from '../../assets/img/cloud.svg';
import partnerIcon from '../../assets/img/partner.svg';
import Wrapper from '../../Components/Wrapper/';
import SendButton from '../../Components/SendButton';
import device from '../../config/device';

const OurCompany = styled.section`
    width:100%;
    height:500px;
    background:url(${companyBG})no-repeat center;
    background-size:cover;
    padding-top:150px;
    @media ${device.xl}{
        height:auto;
        padding-bottom:100px;
    }
`;

const OurCompanyTitle = styled.h1`
    font-size: 48px;
    letter-spacing: 3px;
    color:#fff;
    font-family:firabold;
    @media ${device.lg}{
        font-size: 35px;
    }
`;
const OurCompanyText = styled.p`
    font-size: 24px;
    color:#fff;
    padding-top:20px;
    width:88%;
    line-height:32px;
    font-family:firareg;
    @media ${device.lg}{
        width:100%;
        font-size: 19px;
        line-height:24px;
    }
`;

const Offers = styled(Wrapper)`
    margin-top:-75px;
    display: flex;
    justify-content: space-around;
    align-items: stretch;
    @media ${device.xl}{
        justify-content:space-between;
    }
    @media ${device.lg}{
        margin-top:50px;
        flex-wrap:wrap;
    }
`;
const OffersItem = styled.div`
    font-size:24px;
    color:#051F61;
    display: flex;
    justify-content: flex-start;
    align-items:center;
    flex-direction:column;
    font-family:firabold;
    @media ${device.lg}{
        width:50%;
        margin-bottom:50px;
    }
    @media ${device.md}{
        font-size:18px;
    }
`;
const OffersImg = styled.img`
    width:150px;
    height:150px;
    object-fit:contain;
    margin-bottom:10px;
    @media ${device.md}{
        width:100px;
        height:100px;
    }
`;
const Informative = styled.main`
    width:100%;
    background:url(${BgPart})no-repeat bottom right;
`;

const Work = styled.section`
    padding-top:290px;
    display: flex;
    align-items:flex-start;
    @media ${device.xl}{
        padding-top:190px;
    }
    @media ${device.lg}{
        padding-top:50px;
        flex-direction:column;
    }
`;

const WorkInfo = styled.div`
    width:100%;
`;

const WorkTitle = styled.h1`
    font-size:48px;
    color:#051f61;
    margin-bottom:40px;
    font-family:firabold;
    @media ${device.lg}{
        width:60%;
        font-size: 35px;
    }
    @media ${device.md}{
        width:100%;
    }
`;

const WorkDesc = styled.p`
    font-size:18px;
    font-family:firareg;
    color:#17388a;
    margin-bottom:40px;
    padding:15px 0;
    width:88%;
    margin:0;
    @media ${device.lg}{
        width:100%;
    }
    @media ${device.lg}{
        width:100%;
        font-size: 19px;
        line-height:24px;
    }
`;

const WorkDescYellow = styled.span`
    color: #F2BF52;
`;

const WorkDescBlue = styled.span`
    color: #56BFC9;
`;

const WorkImg = styled.div`
    width:100%;
    height:auto;
    margin-top:40px;
    padding:0 20px;
    .blue-trade{
        animation: lavitate 3s linear infinite alternate;
        @keyframes lavitate{
            0%{
                transform:translate(0,0)
            }
            50%{
                transform:translate(0,-20px)
            }
            100%{
                transform:translate(0,0)
            }
        }
    }
`;

const About = styled.section`
    width:100%;
    padding:100px 0 50px 0;
`;

const AboutTitle = styled.h1`
    font-size:42px;
    color:#051f61;
    margin-bottom:35px;
    font-family:firabold;
    @media ${device.lg}{
        font-size: 35px;
    }
`;

const AboutDesc = styled.p`
    font-size:18px;
    font-family:firareg;
    color:#051F61;
    margin:0;
    margin-bottom:15px;
    &:last-child{
        margin-bottom:0;
    }
    @media ${device.lg}{
        width:100%;
        font-size: 19px;
        line-height:24px;
    }
`;

const Partner = styled.section`
    padding:90px 0;
    display: flex;
    @media ${device.lg}{
        margin:50px 0;
        flex-direction:column;
    }
`;

const PartnerVisaul = styled.div`
    width:100%;
    padding:0 10px;
`;

const PartnerDevise = styled.h2`
    font-size:36px;
    font-family:firareg;
    color:#0B3AB3;
    padding:0 30px;
    padding-top:36px;
    text-align:center;
    @media ${device.xl}{
        padding:36px 10px 0 10px;
    }
`;

const PartnerImg = styled.img`
    width:88%;
    height:auto;
    @media ${device.lg}{
        width:100%;
    }
`;

const PartnerInfo = styled.div`
    width:100%;
    padding:0 10px;
`;

const PartnerTitle = styled.h1`
    margin-top:112px;
    font-size:32px;
    font-family:firareg;
    color:#212529;
    margin-bottom:8px;
    @media ${device.lg}{
        font-size: 28px;
    }
`;

const PartnerItem = styled.div`
    font-size:16px;
    font-family:firareg;
    color:#051f61;
    padding:10px 0;
    padding-left:30px;
    position: relative;
    &:after{
        content: "";
        width: 15px;
        height: 15px;
        background: #051F61;
        border-radius: 50%;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        left: 0;
    }
`;

const Contact = styled.section`
    width:100%;
    display: flex;
    margin-bottom:40px;
    @media ${device.lg}{
        flex-direction:column;
    }
`;
const ContactTitle = styled.h1`
    font-size:48px;
    font-family:firabold;
    color:#051f61;
    @media ${device.lg}{
        margin-bottom:30px;
    }
    @media ${device.lg}{
        font-size: 35px;
    }
`;
const ContactInfo = styled.div`
    width:100%;
`;
const ContactItem = styled.a`
    display: block;
    font-size: 24px;
    text-align: right;
    color: #0B3AB3;
    margin-bottom:16px;
    font-family:firareg;
    @media ${device.lg}{
        font-size: 19px;
    }
    &:last-child{
        margin-bottom:0;
    }
`;

const WrapperExt = styled(Wrapper)`
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    @media ${device.lg}{
        flex-direction:column;
    }
`;

const Feedback = styled.section`
    width:100%;
    background:url(${feedbackBG})no-repeat;
    background-size:cover;
    padding:65px 0;
`;

const Map = styled.iframe`
    width:100%;
    padding:0 15px;
    border:none;
    @media ${device.lg}{
        height:300px;
        margin-bottom:50px;
    }
`;

const FeedbackForm = styled.div`
    width:100%;
    padding:0 15px;
`;

const FeedbackInput = styled.input`
    width:100%;
    border: none;
    outline: none;
    box-shadow: none;
    border-bottom: 2px solid white;
    background: transparent;
    border-radius: 0;
    margin-bottom: 40px;
    font-family:firareg;
    font-size: 18px;
    letter-spacing: 0.15em;
    color: #FFFFFF;
    padding:6px 12px;
    &::placeholder{
        color:#FFFFFF;
    }
`;
const FeedbackTextarea = styled.textarea`
    width:100%;
    border: none;
    outline: none;
    box-shadow: none;
    border-bottom: 2px solid white;
    background: transparent;
    border-radius: 0;
    margin-bottom: 40px;
    font-family:firareg;
    font-size: 18px;
    letter-spacing: 0.15em;
    color: #FFFFFF;
    padding:6px 12px;
    &::placeholder{
        color:#FFFFFF;
    }
`;

const MainPage = () => {
    return(
        <Fragment>
            <Header/>
            <OurCompany>
                <Wrapper>
                    <OurCompanyTitle>
                        ვისთვისაა ჩვენი კომპანია?
                    </OurCompanyTitle>
                    <OurCompanyText>
                        ჩვენ გვჯერა, რომ მეწარმეები გატაცებულები უნდა იყვნენ საყვარელი საქმით და არ უნდა უწევდეთ იმაზე ფიქრი, სად მოიძიონ ფინანსები საკუთარი ბიზნესისთვის.
                        ძიების პროცესი ხშირად საკმაოდ ხანგრძლივი და დამღლელია, ამიტომ ჩვენ გამოვიგონეთ ახალი,
                        უკეთესი და უფრო სწრაფი გზა, რათა დავეხმაროთ ქართულ კომპანიებს დაფინანსების მიღებაში.
                    </OurCompanyText>
                </Wrapper>
            </OurCompany>
            <Informative>
                <Offers>
                    <OffersItem>
                        <OffersImg src={OffersIcon1}/>
                        <span>მომსახურება</span>
                    </OffersItem>
                    <OffersItem>
                        <OffersImg src={OffersIcon2}/>
                        <span>მშენებლობა</span>
                    </OffersItem>
                    <OffersItem>
                        <OffersImg src={OffersIcon3}/>
                        <span>წარმოება</span>
                    </OffersItem>
                    <OffersItem>
                        <OffersImg src={OffersIcon4}/>
                        <span>ვაჭრობა</span>
                    </OffersItem>
                </Offers>
                <Wrapper>
                    <Work id="how-to-work">
                        <WorkInfo>
                            <WorkTitle>ვნახოთ, როგორ ვახერხებთ ამას</WorkTitle>
                            <WorkDesc>
                                მოცემულია გამყიდველი კომპანია <WorkDescYellow>A</WorkDescYellow> და შემსყიდველი კომპანია (<WorkDescBlue>B</WorkDescBlue>).
                            </WorkDesc>
                            <WorkDesc>
                                კომპანია <WorkDescBlue>B</WorkDescBlue>-ს სურს შეისყიდოს ნაწარმი/მომსახურება კომპანია <WorkDescYellow>A</WorkDescYellow>-სგან კრედიტორული დავალიანებით, მაგრამ კომპანია <WorkDescYellow>A</WorkDescYellow> უარზეა.
                            </WorkDesc>
                            <WorkDesc>
                            როგორც მედიატორები, ჩვენ (<WorkDescBlue>Tradeshift</WorkDescBlue>) ვყიდულობთ პროდუქტს/მომსახურებას კომპანია <WorkDescYellow>A</WorkDescYellow>-სგან და მივყიდით მას კრედიტორული დავალიანებით კომპანია <WorkDescBlue>B</WorkDescBlue>-ს. კომპანია <WorkDescYellow>A</WorkDescYellow>-სთან შეთანხმების პროცესში, ჩვენ მას ვაცნობთ ჩვენ სარგებელს და ვეუბნებით, რომ ნებისმიერ დროს, როდესაც კომპანია <WorkDescYellow>A</WorkDescYellow>-ს დასჭირდება რაიმე პროდუქტის/მომსახურების შეძენა, ჩვენ შეგვიძლია მას ფინანსური მხარდაჭერა გავუწიოთ. ამგვარად, კომპანია A-ს შეუძლია გახდეს კომპანია <WorkDescBlue>B</WorkDescBlue>.
                            </WorkDesc>
                        </WorkInfo>
                        <WorkImg>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 333 250" fill="none">
                                <g className="blue-trade">
                                <path d="M140.883 176.01C154.017 189.144 175.313 189.144 188.447 176.01C201.582 162.875 201.582 141.58 188.447 128.445C175.313 115.311 154.017 115.311 140.883 128.445C127.748 141.58 127.748 162.875 140.883 176.01Z" fill="#078FE2"/>
                                <path d="M197.558 184.052C190.728 190.909 181.819 195.311 172.223 196.57C173.252 197.561 174.58 198.184 176.001 198.339L208.26 202.038C209.244 202.152 210.24 202.041 211.176 201.715C212.111 201.389 212.961 200.856 213.661 200.155C214.361 199.455 214.894 198.606 215.22 197.67C215.546 196.735 215.657 195.738 215.544 194.754L211.841 162.492C211.683 161.072 211.061 159.744 210.072 158.714C208.814 168.311 204.414 177.221 197.558 184.052Z" fill="#078FE2"/>
                                <path d="M131.95 118.623C138.781 111.766 147.689 107.364 157.285 106.105C156.258 105.112 154.928 104.489 153.507 104.336L121.249 100.637C120.265 100.523 119.268 100.634 118.332 100.96C117.397 101.286 116.548 101.819 115.847 102.52C115.147 103.22 114.614 104.069 114.288 105.005C113.962 105.94 113.851 106.937 113.964 107.921L117.667 140.176C117.821 141.597 118.442 142.927 119.433 143.957C120.695 134.362 125.096 125.454 131.95 118.623Z" fill="#078FE2"/>
                                <path d="M131.95 184.052C125.094 177.222 120.692 168.314 119.433 158.718C118.442 159.748 117.821 161.078 117.667 162.499L113.964 194.754C113.851 195.738 113.961 196.735 114.287 197.67C114.614 198.606 115.147 199.455 115.847 200.156C116.547 200.856 117.397 201.389 118.332 201.715C119.267 202.041 120.264 202.152 121.248 202.038L153.507 198.339C154.928 198.186 156.257 197.563 157.285 196.57C147.689 195.311 138.78 190.909 131.95 184.052Z" fill="#078FE2"/>
                                <path d="M197.558 118.623C204.415 125.453 208.816 134.362 210.076 143.957C211.066 142.926 211.689 141.597 211.848 140.176L215.533 107.932C215.647 106.948 215.536 105.951 215.21 105.015C214.884 104.08 214.351 103.231 213.65 102.53C212.95 101.83 212.101 101.297 211.165 100.971C210.23 100.645 209.233 100.534 208.249 100.647L175.997 104.346C174.576 104.504 173.246 105.127 172.216 106.119C181.813 107.373 190.724 111.77 197.558 118.623Z" fill="#078FE2"/>
                                <path d="M168.138 168.913H161.645V143.693H153.329V138.289H176.454V143.693H168.138V168.913Z" fill="white"/>
                                </g>


                                <path d="M62.345 62.3415C55.5149 69.1982 46.6064 73.5999 37.0106 74.8591C38.0421 75.8493 39.3708 76.4724 40.7916 76.6321L73.0363 80.3169C74.0203 80.4302 75.0171 80.3197 75.9524 79.9936C76.8877 79.6676 77.7373 79.1345 78.4376 78.4341C79.138 77.7337 79.6711 76.8842 79.9972 75.9489C80.3232 75.0136 80.4337 74.0168 80.3205 73.0328L76.6214 40.7809C76.4638 39.3595 75.8405 38.0301 74.8484 37C73.5946 46.5965 69.1979 55.5075 62.345 62.3415Z" fill="#F2BF52"/>
                                <circle cx="33.5145" cy="33.5145" r="33.5145" fill="#F2BF52"/>
                                <path d="M40.2592 47.7246L38.2609 41.1642H28.2128L26.2145 47.7246H19.918L29.6456 20.05H36.7904L46.5557 47.7246H40.2592ZM36.8658 36.2627C35.0183 30.3181 33.9752 26.9561 33.7364 26.1769C33.5102 25.3977 33.3468 24.7819 33.2463 24.3294C32.8315 25.9381 31.6439 29.9159 29.6833 36.2627H36.8658Z" fill="white"/>
                                <path d="M270.961 62.345C264.104 55.515 259.702 46.6064 258.443 37.0107C257.453 38.0421 256.83 39.3708 256.67 40.7916L252.985 73.0363C252.872 74.0203 252.983 75.0172 253.309 75.9525C253.635 76.8878 254.168 77.7373 254.868 78.4377C255.568 79.1381 256.418 79.6712 257.353 79.9972C258.289 80.3232 259.285 80.4337 260.269 80.3205L292.521 76.6214C293.943 76.4639 295.272 75.8405 296.302 74.8485C286.706 73.5946 277.795 69.1979 270.961 62.345Z" fill="#56BFC9"/>
                                <circle cx="299.486" cy="33.5145" r="33.5145" fill="#56BFC9"/>
                                <path d="M290.252 20.1632H298.83C302.739 20.1632 305.573 20.7224 307.332 21.841C309.104 22.947 309.99 24.7128 309.99 27.1384C309.99 28.7848 309.601 30.1358 308.822 31.1915C308.055 32.2472 307.031 32.8819 305.749 33.0956V33.2841C307.496 33.6737 308.752 34.4026 309.519 35.4709C310.298 36.5392 310.688 37.9594 310.688 39.7314C310.688 42.245 309.777 44.2056 307.954 45.6132C306.145 47.0208 303.681 47.7246 300.564 47.7246H290.252V20.1632ZM296.097 31.0784H299.49C301.073 31.0784 302.217 30.8333 302.921 30.3432C303.637 29.853 303.995 29.0424 303.995 27.9113C303.995 26.8556 303.606 26.1015 302.827 25.6491C302.06 25.1841 300.841 24.9515 299.169 24.9515H296.097V31.0784ZM296.097 35.716V42.8986H299.905C301.513 42.8986 302.701 42.5906 303.468 41.9748C304.234 41.359 304.618 40.4164 304.618 39.147C304.618 36.8597 302.984 35.716 299.716 35.716H296.097Z" fill="white"/>
                            </svg>
                        </WorkImg>
                    </Work>
                    <About id="about-us">
                        <AboutTitle>ჩვენ შესახებ</AboutTitle>
                        <AboutDesc>Tradeshift-ში გვჯერა, რომ ბიზნესის ზრდას დიდი გლობალური სარგებლის მოტანა შეუძლია. სწორედ ამიტომ, პირველივე დღიდან მიზნად დავისახეთ მეწარმეთათვის მათი ბიზნესის დაფინანსება გაგვემარტივებინა.</AboutDesc>
                        <AboutDesc>ჩვენი სხარტი და მოტივირებული გუნდი გამოირჩევა განსხვავებული აზროვნებით და მის წევრებს არ ეშინიათ გააკეთონ ის, რაც აქამდე არავის გაუბედავს.</AboutDesc>
                        <AboutDesc>Tradeshift კლიენტებს მომსახურების ფართო სპექტრს ერთ სივრცეში სთავაზობს, მათ შორის - ყოველდღიურ, ყოველკვირეულ ან ყოველთვიურ ვაჭრობას.</AboutDesc>
                        <AboutDesc>გვწამს, რომ მეწარმეები გატაცებულები უნდა იყვნენ საყვარელი საქმით და არ უნდა უწევდეთ იმაზე ფიქრი, სად მოიძიონ ფინანსები საკუთარი ბიზნესისთვის. ძიების პროცესი ხშირად საკმაოდ ხანგრძლივი და დამღლელია, ამიტომ ჩვენ გამოვიგონეთ ახალი, უკეთესი და უფრო სწრაფი გზა ქართული კომპანიების დასაფინანსებლად. ჩვენი მენეჯმენტის სისტემისა და გუნდის დახმარებით, რომელიც ერთი შეხედვით შეუძლებელსაც კი შესაძლებელს ხდის, ჩვენ ვეხმარებით ბიზნესებს, სწრაფად და მარტივად განკარგონ მათი ფულადი სახსრების დინება.</AboutDesc>
                    </About>
                    <Partner id="partnership">
                        <PartnerVisaul>
                            <PartnerImg src={partnerIcon}/>
                            <PartnerDevise>Tradeshift - “მიეცით თქვენს ბიზნესს სწორი მიმართულება”</PartnerDevise>
                        </PartnerVisaul>
                        <PartnerInfo>
                            <PartnerTitle>ჩვენთან თანამშრომლობისას</PartnerTitle>
                            <PartnerItem>არ გექნებათ ნაღდი ფულის პრობლემა</PartnerItem>
                            <PartnerItem>არ გექნებათ შიშის ან უნდობლობის გრძნობა</PartnerItem>
                            <PartnerItem>თქვენი ბიზნესი არ შეფერხდება</PartnerItem>
                            <PartnerItem>არ დაკარგავთ ახალ კლიენტებსა და დამატებით შემოსავალს</PartnerItem>
                            <PartnerItem>დაზოგავთ თანხებსა და ენერგიას</PartnerItem>
                        </PartnerInfo>
                    </Partner>
                    <Contact id="contact-us">
                        <ContactTitle>დაგვიკავშირდით</ContactTitle>
                        <ContactInfo>
                            <ContactItem href="tel:+0322 19 44 94">+0322 19 44 94</ContactItem>
                            <ContactItem href="mailto:hello@tradeshift.ge">hello@tradeshift.ge</ContactItem>
                            <ContactItem>თბილისი, ვაჟა-ფშაველას გამზ. 45<br/> (მე-9 სართული).</ContactItem>
                        </ContactInfo>
                    </Contact>
                </Wrapper>
                <Feedback>
                    <WrapperExt>
                        <Map src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2977.757625391687!2d44.741432915882584!3d41.725748679234655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4044731ac26430eb%3A0x271f61fe8e5f9edf!2z4YOV4YOQ4YOf4YOQLeGDpOGDqOGDkOGDleGDlOGDmuGDkOGDoSAxIOGDmeGDlS4sIDQ1IFZhemhhIFBzaGF2ZWxhIEF2ZSwg4YOX4YOR4YOY4YOa4YOY4YOh4YOY!5e0!3m2!1ska!2sge!4v1555686416826!5m2!1ska!2sge"></Map>
                        <FeedbackForm>
                            <FeedbackInput type="text" placeholder="სახელი და გვარი"/>
                            <FeedbackInput type="email" placeholder="ელ. ფოსტა"/>
                            <FeedbackTextarea cols="30" rows="7" placeholder="თქვენი შეტყობინება"/>
                            <SendButton>გაგზავნა</SendButton>
                        </FeedbackForm>
                    </WrapperExt>
                </Feedback>
            </Informative>
        </Fragment>
    )
}

export default MainPage;