import styled from 'styled-components';

const SendButton = styled.button`
    padding: 10px 50px;
    background: none;
    box-shadow: none;
    border: 2px solid white;
    color: white;
    margin-top: 20px;
    font-family: firabold;
    font-size: 18px;
    text-align: center;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    outline:none;
    cursor: pointer;
`;

export default SendButton;