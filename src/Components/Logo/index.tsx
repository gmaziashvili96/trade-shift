import styled from 'styled-components';
import logoIcon from '../../assets/img/logo-white.png';

const Logo = styled.div`
   width:235px;
   height:38px;
   background:${props => props.color || `url(${logoIcon})no-repeat center`};
   background-size:contain;
`;

export default Logo;