import styled from 'styled-components';
import device from '../../config/device';

const Wrapper = styled.div`
    width:100%;
    max-width: 1140px;
    padding:0 15px;
    margin:0 auto;
    @media ${device.xl}{
        max-width:960px;
    }
    @media ${device.lg}{
        max-width:720px;
    }
    @media ${device.md}{
        max-width: 576px;
    }
`;

export default Wrapper;