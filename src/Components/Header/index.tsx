import styled from 'styled-components';
import Wrapper from '../Wrapper';
import Logo from '../Logo';
import facebookIcon from '../../assets/img/fb__lan.svg';
import enIcon from '../../assets/img/en__lan.svg';
import facebookIconDark from '../../assets/img/fb.svg';
import enIconDark from '../../assets/img/en.svg';
import React from 'react';
import logoIconLight from '../../assets/img/logo-white.png';
import logoIconDark from '../../assets/img/logo-dark.png';
import device from '../../config/device';
import menuIcon from '../../assets/img/menu.svg';

const Menu = styled.div`
    width:30px;
    height:30px;
    background:url(${menuIcon})no-repeat center;
    background-size:contain;
    display: none;
    cursor:pointer;
    @media ${device.xl}{
        display: block;
    }
`;


const WrapperExt = styled(Wrapper)`
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 2px solid rgba(255, 255, 255, 0.4);
    background:${props => props.color || "#fff"};
    transition:0.3s all;
    box-shadow:rgba(0, 0, 0, 0.15) 2px 2px 10px;
    @media ${device.xl}{
        flex-wrap:wrap;
        padding:15px;
    }
`;

const HeaderStyle = styled.header`
    position:fixed;
    top:0;
    left:0;
    width:100%;
    height:68px;
    padding-left:15px;
    display: flex;
    justify-content: center;
    align-items: stretch;
    margin-top:15px;
    z-index:9999;
    @media ${device.xl}{
        flex-wrap:wrap;
        height:auto;
    }
`;

const SmallWrapper = styled.div`
    display: flex;
    align-items: center;
    @media ${device.xl}{
        overflow:hidden;
        transition:0.3s all;
        width:100%;
        flex-direction:column;
        justify-content:space-between;
        height:${props => (props.color ? props.color  : `0px`)}
    }
`;

const NavBar = styled.nav`
    display: flex;
    align-items: center;
    @media ${device.xl}{
        flex-direction:column;
        height: 100%;
        justify-content:space-around;
    }
`;
const NavItem = styled.a`
    padding:0 14px;
    font-size:16px;
    color:${props => props.color || "transparent"};
    font-family:firareg;
    cursor:pointer;
    @media ${device.xl}{
        margin-bottom:15px;
    }
`;

const SocialTag = styled.a`
    margin:0 10px;
    width:25px;
    height:25px;
`;

const FbIcon = styled(SocialTag)`
    background:url(${facebookIcon})no-repeat center;
    background:${props => props.color || `url(${facebookIcon})no-repeat center`};
    background-size:contain;
`;

const LangIcon = styled(SocialTag)`
    background:url(${enIcon})no-repeat center;
    background:${props => props.color || `url(${enIcon})no-repeat center`};
    background-size:100% 100%;
`;

const SocialTagWrp = styled.div`
    display: flex;
    align-items: center;
`;

class Header extends React.Component {
    state = {
      isTop: true,
      isOpen:false,
    };

    scrollHeader = () => {
        const isTop = window.scrollY < 1;
        console.log(isTop)
        if (isTop !== this.state.isTop) {
            this.setState({ isTop })
        }
    }

    menuToggleHandler = () => {
        var isOpen = this.state.isOpen;
        this.setState({
            isOpen:!isOpen,
            isTop: false,
        })
    }
    closeMenuHandler = () => {
        this.setState({
            isOpen:false,
        })
    }
    componentDidMount() {
        document.addEventListener('scroll', this.scrollHeader);
    }
    componentWillUnmount() {
        document.removeEventListener('scroll', this.scrollHeader);
    }
    render() {
        return(
            <HeaderStyle>
                <WrapperExt color={this.state.isTop ? 'transparent':' #fff'}>
                    <Logo color={this.state.isTop ? `url(${logoIconLight})no-repeat center`:`url(${logoIconDark})no-repeat center`}/>
                    <Menu onClick={this.menuToggleHandler}/>
                    <SmallWrapper color={this.state.isOpen ? '200px' : '0px'}>
                        <NavBar>
                            <NavItem onClick={this.closeMenuHandler} href="#how-to-work" color={this.state.isTop ? '#fff': 'rgb(7, 134, 226)'}>როგორ ვმუშაობთ</NavItem>
                            <NavItem onClick={this.closeMenuHandler} href="#about-us" color={this.state.isTop ? '#fff': 'rgb(7, 134, 226)'}>ჩვენ შესახებ</NavItem>
                            <NavItem onClick={this.closeMenuHandler} href="#partnership" color={this.state.isTop ? '#fff': 'rgb(7, 134, 226)'}>პარტიორობა</NavItem>
                            <NavItem onClick={this.closeMenuHandler} href="#contact-us" color={this.state.isTop ? '#fff': 'rgb(7, 134, 226)'}>დაგვიკავშირდით</NavItem>
                        </NavBar>
                        <SocialTagWrp>
                            <LangIcon color={this.state.isTop ? `url(${enIcon})no-repeat center`:`url(${enIconDark})no-repeat center`}/>
                            <FbIcon color={this.state.isTop ? `url(${facebookIcon})no-repeat center`:`url(${facebookIconDark})no-repeat center`}/>
                        </SocialTagWrp>
                    </SmallWrapper>
                </WrapperExt>
            </HeaderStyle>
        )
    }
} 


export default Header;